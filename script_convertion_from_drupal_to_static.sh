#!/bin/bash

# Script to create static Website for FSL 2024

# Step 1: Uninstall modules incompatible with the Tome module
echo "Step 1: Uninstalling modules incompatible with the Tome module..."
drush un masquerade big_pipe_sessionless redis
echo "Incompatible modules uninstalled successfully."

# Step 2: Install Tome for static site generation
echo "Step 2: Installing the Tome module for static site generation..."
drush en tome_static
echo "Tome module installed successfully."

# Step 3: Build static site with Tome
echo "Step 3: Building the static site with Tome..."
drush tome:static --uri https://festa2024.softwarelivre.eu -vvv
echo "Static site built successfully."

# Step 4: Remove all RSS links for '/pt-pt/rss.xml'
echo "Step 4: Removing all RSS links for '/pt-pt/rss.xml'..."
grep -rl '/pt-pt/rss.xml' . | xargs -I {} sh -c 'echo "Processing {} to remove /pt-pt/rss.xml"; sed -i "s/\/pt-pt\/rss.xml//g" "{}"'
echo "All '/pt-pt/rss.xml' links removed successfully."

# Step 5: Remove all RSS links for '/en/rss.xml'
echo "Step 5: Removing all RSS links for '/en/rss.xml'..."
grep -rl '/en/rss.xml' . | xargs -I {} sh -c 'echo "Processing {} to remove /en/rss.xml"; sed -i "s/\/en\/rss.xml//g" "{}"'
echo "All '/en/rss.xml' links removed successfully."

# Script completed
echo "Static website generation for FSL 2024 completed successfully."

